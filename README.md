California Pools is one of the largest pool builders in the country and brings award-winning custom pools to the Corona area. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 1307 W 6th St, #122, Corona, CA 92882, USA

Phone: 951-283-3134

Website: https://www.californiapools.com/locations/corona
